//
//  LocalGateway.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 18/03/22.
//

import Foundation
import RealmSwift

class LocalGateway {
    
    func writeCatRealm(object: CatRealmItem) {
        
        do {
            
            try realm.write {
                
                let catRealm = CatRealm()
                
                catRealm.id = object.id
                catRealm.name = object.name
                catRealm.like = object.like
                catRealm.disLike = object.disLike
                catRealm.date = object.date
                
                realm.add(catRealm)
            }
            
        } catch {
            print("Error adding to Realm Cat", error)
        }
    }
    
    func readCatRealm(name: String) -> CatRealmItem? {
        
        let catRealm = realm.objects(CatRealm.self).filter("name = %@", name).first
       
        if catRealm != nil {
            
            return CatRealmItem(id: catRealm?.id ?? 1, name: catRealm?.name ?? "", like: catRealm?.like ?? 0, disLike: catRealm?.disLike ?? 0, date: catRealm?.date ?? "")
            
        } else {
            
            return nil
        }
    }
    
    func lastObjectID() -> Int {
        
        let lastObject = realm.objects(CatRealm.self).last
        
        if lastObject != nil {
         
            return lastObject?.id ?? 0
            
        } else {
            
            return 0
        }
    }
    
    func updateCatRealm(object: CatRealmItem) {
        
        do {
            
            try realm.write {
                
                guard let catRealm = realm.objects(CatRealm.self).filter("name = %@", object.name).first else {return}
                
                catRealm.id = object.id
                catRealm.name = object.name
                catRealm.like = object.like
                catRealm.disLike = object.disLike
                catRealm.date = object.date
                
                realm.add(catRealm)
            }
            
        } catch {
            print("Error update to Realm Cat", error)
        }
    }
}
