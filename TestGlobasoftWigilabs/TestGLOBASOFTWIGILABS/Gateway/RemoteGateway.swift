//
//  RemoteGateway.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 17/03/22.
//

import Foundation
import Alamofire

class RemoteGateway {
    
    let decoder = JSONDecoder()
    
    //MARK: Consumo rest de esta api https://api.thecatapi.com
    func getPet(successCallback: @escaping (PetDTO) -> Void, errorCallback: @escaping (NSDictionary) -> Void, networkErrorCallback: @escaping (AFError) -> Void) {
        
        APIRequests.basicRequest(url: "https://api.thecatapi.com", body: nil, headers: [], method: .get, successCallback:{ data in
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: data, options: [])
                let decodedElement = try self.decoder.decode(PetDTO.self, from: jsonData)

                successCallback(decodedElement)
                
            } catch {
                print("error saveCard catch: ", error)

            }
            
        }, errorCallback: { error in
            
            errorCallback(error)
            
        }, networkErrorCallback: { error in
            
            networkErrorCallback(error)
        })
    }
}
