//
//  APIConstants.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 17/03/22.
//

import Foundation
import Alamofire

//MARK: APIRequests
struct APIRequests {
    
    static func basicRequest (preLogin: Bool = false, url: String, body: [String: Any]?, headers: HTTPHeaders, method: HTTPMethod, successCallback: @escaping (NSDictionary) -> Void, errorCallback: @escaping (NSDictionary) -> Void, networkErrorCallback: @escaping (AFError) -> Void) {
        
        AF.request("\(url)", method: method, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseData { response in
                
                switch response.result {
                    
                case .success(let data):
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: data)
                        
                        guard let decode = json as? NSDictionary else {return}
                        let statusCode = response.response?.statusCode ?? 0
                        
                        switch statusCode {
                            
                        case 200..<300:
                            
                            successCallback(decode)
                            
                        default:
                            errorCallback(decode)
                        }
                        
                    } catch { print("erroMsg") }
                    
                case .failure(let error):
                    networkErrorCallback(error)
                }
            }
    }
}
