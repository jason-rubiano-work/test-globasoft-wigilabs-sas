//
//  CatDetailsView.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 18/03/22.
//

import SwiftUI

struct CatDetailsView: View {
    
    @Binding var modal: Bool
    @State var catName: String
    @State var voteArray: [String]
    
    var body: some View {
        
        VStack(spacing: 16) {
            
            HStack(spacing: 0) {

                Spacer()

                Button(action: {

                    modal.toggle()
                }) {

                    Image(systemName: "xmark")
                        .foregroundColor(.blue)
                        .imageScale(.medium)
                }

            }.padding(.top, 16)
            
            Image(catName)
                .resizable()
                .cornerRadius(8)
                .aspectRatio(contentMode: .fit)
                .frame(maxWidth: .infinity)
            
            Text(catName)
            
            List(voteArray, id: \.self) { item in
                
                HStack(spacing: 0) {
                    
                    Text("\(item.components(separatedBy: "-")[0])")
                    
                    Spacer()
                    
                    Text("\(item.components(separatedBy: "-")[1])")
                }
            }
            
        }.padding(.horizontal, 22)
    }
}

struct CatDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        CatDetailsView(modal: .constant(false), catName: "Cat1", voteArray: [])
    }
}
