//
//  ContentView.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 17/03/22.
//

import SwiftUI
import Alamofire

struct ContentView: View {
    
    @State var petDTO: PetDTO = PetDTO()
    
    let columns = [
            GridItem(.flexible()),
            GridItem(.flexible()),
    ]
    
    var body: some View {
        
        VStack {
            
            Spacer()
            
            ScrollView(.vertical, showsIndicators: false) {
                
                LazyVGrid(columns: columns, spacing: 20) {
                    
                    ForEach(Catlist, id: \.id) { item in
                        
                        CatCard(catItem: item)
                    }
                    
                }.padding(.vertical)
            }
            
            Spacer()
            
            HStack(spacing: 0) {
                
                Text(self.petDTO.message ?? "")
                
                Spacer()
                
                Text(self.petDTO.version ?? "")
            }
            
        }.padding([.horizontal, .bottom], 16)
            .onAppear() {
                
                RemoteGateway().getPet(successCallback: { data in
                    
                    self.petDTO = data
                    
                }, errorCallback: { error in }, networkErrorCallback: { error in })
            }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(petDTO: PetDTO(message: "Dog", version: "1.0.0"))
            .environmentObject(CatRealmObserver())
    }
}
