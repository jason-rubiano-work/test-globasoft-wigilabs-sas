//
//  CatItem.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 17/03/22.
//

import Foundation

struct CatItem: Identifiable {
    var id: Int
    var image: String
    var like: Bool
}

let Catlist: [CatItem] = [
    .init(id: 1, image: "Cat1", like: false),
    .init(id: 2, image: "Cat2", like: false),
    .init(id: 3, image: "Cat3", like: false),
    .init(id: 4, image: "Cat4", like: false)
]
