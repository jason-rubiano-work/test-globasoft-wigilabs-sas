//
//  TestGLOBASOFTWIGILABSApp.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 17/03/22.
//

import RealmSwift
import SwiftUI

let realm = try! Realm()

@main
struct TestGLOBASOFTWIGILABSApp: SwiftUI.App {
    
    @StateObject var catRealmObserver = CatRealmObserver()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.realmConfiguration, Realm.Configuration(schemaVersion: 1))
                .environmentObject(catRealmObserver)
        }
    }
}
 
