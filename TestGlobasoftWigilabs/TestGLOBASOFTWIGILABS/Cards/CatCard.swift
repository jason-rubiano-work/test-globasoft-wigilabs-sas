//
//  CatCard.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 17/03/22.
//

import SwiftUI

struct CatCard: View {
    
    @EnvironmentObject var catRealmObserver: CatRealmObserver
    
    var catItem: CatItem
    
    @State var voteArray: [String] = []
    
    @State var modalDetails: Bool = false
    
    var body: some View {
        
        VStack(spacing: 16) {
            
            Image(catItem.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 140)
                .cornerRadius(8)
                .onTapGesture {
                    
                    self.modalDetails.toggle()
                }
            
            HStack(spacing: 0) {
                
                Button(action: {
                    
                    let catRealmItem = catRealmObserver.cat.filter("name = %@", catItem.image).first
                    voteArray = catRealmItem?.date?.components(separatedBy: ",") ?? []
                    voteArray.append("Like-\(getDate())")
                    
                    if catRealmItem == nil {
                        
                        LocalGateway().writeCatRealm(object: CatRealmItem(id: 1, name: catItem.image, like: 1, disLike: 0, date: voteArray.joined(separator: "")))
                    } else {
                        
                        LocalGateway().updateCatRealm(object: CatRealmItem(id: (catRealmItem?.id ?? 0) + 1, name: catItem.image, like: (catRealmItem?.like ?? 0) + 1, disLike: catRealmItem?.disLike ?? 0, date: voteArray.joined(separator: ",")))
                    }
                    
                }) {
                 
                    VStack(spacing: 5) {
                     
                        Image(systemName: "hand.thumbsup.fill")
                            .foregroundColor(.blue)
                            .imageScale(.large)
                        
                        Text("\(catRealmObserver.cat.filter("name = %@", catItem.image).first?.like ?? 0)")
                    }
                }
                
                Spacer()
                
                Button(action: {
                    
                    let catRealmItem = catRealmObserver.cat.filter("name = %@", catItem.image).first
                    voteArray = (catRealmItem?.date ?? "").components(separatedBy: ",")
                    voteArray.append("Dislike-\(getDate())")
                    
                    if catRealmItem == nil {
                        
                        LocalGateway().writeCatRealm(object: CatRealmItem(id: 1, name: catItem.image, like: 0, disLike: 1, date: voteArray.joined(separator: "")))
                    } else {
                        
                        LocalGateway().updateCatRealm(object: CatRealmItem(id: (catRealmItem?.id ?? 0) + 1, name: catItem.image, like: catRealmItem?.like ?? 0, disLike: (catRealmItem?.disLike ?? 0) + 1, date: voteArray.joined(separator: ",")))
                    }
                    
                }) {
                 
                    VStack(spacing: 5) {
                     
                        Image(systemName: "hand.thumbsup")
                            .foregroundColor(.blue)
                            .imageScale(.large)
                            .rotationEffect(.radians(.pi))
                        
                        Text("\(catRealmObserver.cat.filter("name = %@", catItem.image).first?.disLike ?? 0)")
                    }
                }
                
            }.padding(.horizontal, 20)
                .padding(.bottom, 16)
            
        }.frame(width: 140)
            .background(Color.white)
            .cornerRadius(8)
            .shadow(color: Color.black.opacity(0.2), radius: 5)
            .sheet(isPresented: $modalDetails) {
                CatDetailsView(modal: $modalDetails, catName: catItem.image, voteArray: LocalGateway().readCatRealm(name: catItem.image)?.date.components(separatedBy: ",") ?? [])
                    //.environmentObject(self.catRealmObserver)
            }
    }
    
    func getDate() -> String {
        
        let date = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss dd/MM/yyyy"
        return dateFormatter.string(from: date)
    }
}

struct CatCard_Previews: PreviewProvider {
    static var previews: some View {
        CatCard(catItem: CatItem(id: 1, image: "Cat1", like: false))
            .environmentObject(CatRealmObserver())
    }
}
