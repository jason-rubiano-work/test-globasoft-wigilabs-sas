//
//  CatRealm.swift
//  TestGLOBASOFTWIGILABS
//
//  Created by JASS on 18/03/22.
//

import Foundation
import RealmSwift

// Realm Model
class CatRealm: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var like: Int = 0
    @objc dynamic var disLike: Int = 0
    @objc dynamic var date: String?
}

// UI Model
struct CatRealmItem: Identifiable {
    var id: Int = 1
    var name: String = ""
    var like: Int = 0
    var disLike: Int = 0
    var date: String  = ""
}

final class CatRealmObserver: ObservableObject {

    @Published var cat: Results<CatRealm>
    private var objectsToken: NotificationToken? = nil
    
    init() {
        cat = realm.objects(CatRealm.self)
        activateObjectsToken()
    }
    
    private func activateObjectsToken() {
        
        let objects = realm.objects(CatRealm.self)
        objectsToken = objects.observe { _ in
            
            self.cat = objects
        }
    }
}
